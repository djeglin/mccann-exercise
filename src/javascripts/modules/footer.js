import {default as utils} from "../_utils";

export default class Footer {
  constructor(el) {
    this.el = el;
    this.init();
  }

  init() {
    this.bindEvents();
  }

  bindEvents() {
    const that = this,
          newsletterInput = this.el.querySelector('input.newsletter-email');

    utils.addEvent(newsletterInput, "focus", function() {
      that.hideLabel(newsletterInput);
    });
  }

  hideLabel(input) {
    const label = input.parentNode.querySelector('label');
    label.classList.add('hidden');
  }

}