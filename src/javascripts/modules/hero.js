import {default as utils} from "../_utils";

export default class Hero {
  constructor(el) {
    this.el = el;
    this.init();
  }

  init() {
    this.options = {
      itemTime: 5000,
      progressTime: 10
    };
    this.createProgressBar();
    this.createLinks();
    this.startTimer();
    this.bindEvents();
  }

  createProgressBar() {
    const that = this;
    let progressBar = document.createElement('span');
    progressBar.className = "progress";
    this.el.appendChild(progressBar);
    this.progressInterval = setInterval(function() {
      that.updateProgressBar();
    }, that.options.progressTime);
  }

  updateProgressBar() {
    if (this.timer) {
      let progressBar = this.el.querySelector(".progress");
      const percent = this.currentProgress;
      progressBar.style.width = percent + "%";
    }
  }

  createLinks() {
    let numItems = this.el.querySelectorAll('.item').length;
    let itemLinks = document.createElement('ul');
    itemLinks.className = "item-links";
    for (let i = 0, max = numItems; i < max; i++) {
      let thisLink = document.createElement('li');
      thisLink.innerHTML = "" + (i+1);
      if (i === 0) {
        thisLink.classList.add("active");
      }
      itemLinks.appendChild(thisLink);
    }
    this.el.appendChild(itemLinks);
  }

  startTimer() {
    if (!this.timer) {
      const that = this,
            countdown = that.options.itemTime;

      this.itemChangeTime = Number(new Date());
      this.itemDuration = countdown;

      this.timer = setTimeout(function(){
        that.nextItem();
      }, countdown);
    }
  }

  stopTimer() {
    this.itemStopTime = Number(new Date());
    clearTimeout(this.timer);
    this.timer = false;
  }

  gotoItem(item) {
    this.timer = false;
    const targetItem = this.el.querySelector('.item:nth-child(' + item + ')'),
          itemLinks = this.el.querySelector('.item-links'),
          activeLink = itemLinks.querySelector('.active'),
          targetLink = itemLinks.querySelector('li:nth-child(' + item + ')');
    this.currentItem.classList.remove("current");
    targetItem.classList.add("current");
    activeLink.classList.remove('active');
    targetLink.classList.add('active');
  }

  nextItem() {
    this.timer = false;
    const next = this.nextCurrentItem,
          nextLink = this.nextCurrentLink;
    this.currentItem.classList.remove("current");
    next.classList.add("current");
    this.currentLink.classList.remove("active");
    nextLink.classList.add("active");
    this.startTimer();
  }

  bindEvents() {
    const that = this,
          itemLinks = this.el.querySelectorAll(".item-links li");

    utils.addEvent(this.el, "mouseenter", function() {
      that.stopTimer();
    });
    utils.addEvent(this.el, "mouseleave", function() {
      that.startTimer();
    });
    for (let l = 0, max = itemLinks.length; l < max; l++) {
      utils.addEvent(itemLinks[l], "click", function(e) {
        const linkNumber = parseInt(e.target.innerText, 10);
        that.gotoItem(linkNumber);
      });
    }
  }

  get currentItem() {
    return this.el.querySelector(".current");
  }

  get nextCurrentItem() {
    return this.el.querySelector(".current + .item") || this.el.querySelector(".item:first-child");
  }

  get currentLink() {
    return this.el.querySelector(".item-links .active");
  }

  get nextCurrentLink() {
    return this.el.querySelector(".item-links .active + li") || this.el.querySelector(".item-links li:first-child");
  }

  get currentProgress() {
    const now = Number(new Date()),
          elapsed = now - this.itemChangeTime,
          percent = (elapsed / this.itemDuration) * 100;

    return percent;
  }

  get remainingTime() {
    const now = Number(new Date()),
          elapsed = now - this.itemChangeTime,
          remaining = this.options.itemDuration - elapsed;

    return remaining;
  }
}