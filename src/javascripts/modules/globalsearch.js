import {default as utils} from "../_utils";

export default class Globalsearch {
  constructor(el) {
    this.el = el;
    this.init();
  }

  init() {
    this.bindEvents();
  }

  toggleSearch() {
    const input = this.el.querySelector('.search-term');
    if (this.searchIsActive) {
      return;
    }
    this.el.setAttribute("data-search-active", "true");
    input.focus();
  }

  hideLabel() {
    this.el.querySelector('label').classList.add('hidden');
  }

  get searchIsActive() {
    return this.el.hasAttribute("data-search-active");
  }

  bindEvents() {
    const that = this,
          input = this.el.querySelector('input[type="text"]');
    utils.addEvent(this.el, "click", function() {
      that.toggleSearch();
    });
    utils.addEvent(input, "keypress", function() {
      that.hideLabel();
    });
  }
}