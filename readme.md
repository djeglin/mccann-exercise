# McCann Central technical exercise
This repository contains my execution of the provided technical exercise from McCann Central. This readme file is provided to give some insight into the project scaffold and method.

## A note on implementation
I made a concious decisiion not to look at the Crown website, because I didn't want it to influence my decision making process. I didn't want to be trying to replicate their interactions etc. 

## The scaffold
This project was scaffolded with [Blendid](https://github.com/vigetlabs/blendid) for pure speed, as it fits well with McCann's current front-end stack, making use of sass and webpack, and adds in twig-like html templating through [Nunjucks](https://mozilla.github.io/nunjucks/). As Blendid uses [Yarn](https://yarnpkg.com/lang/en/) as the base package manager rather than NPM, there are a couple of differences in terms of the base file structure there, but nothing too significant. 

It is worth noting that, as personal preference for pure speed, I probably would have used [Stylus](http://stylus-lang.com) for CSS preprocessing, and [Coffeescript](http://coffeescript.org) for JS, but coffeescript is losing popularity now in favour of ES5/6, and Stylus, whilst wonderful for those of us who like the whitespace-based, ruby-like syntax, isn't that popular. 

More recently I have been working with [PostCSS](http://postcss.org) with [SugarSS](https://github.com/postcss/sugarss) to get the same ruby-like syntax and [CSSNext](http://cssnext.io/) to enable next-generation CSS syntaxes, all through the [Spike](https://www.spike.cf) static-site generator. The choice of scaffold here was purely to make this project as close to McCann's own internal front-end stack as possible at speed.

## My approach
My philosophy when developing is generally to try and make the html of the page as lean as it can possibly be. I don't like extra markup purely for styling purposes unless it is absolutely necessary. You will see from my CSS work that I make frequent use of `:before` pseudo-elements etc for styling purposes so as to leave the html clean. 

Javascript modules are automatically instantiated on components that use them with the use of a data-attribute. The JS is all properly object-oriented and sandboxed so that it doesn't collide with anything else on the page.

I chose to develop the header and footer components mostly because these would be the first components I would approach if I were developing a site for real. It's much easier to get the header and footer at least partially sorted out first and then move on to fill the pages, rather than developing in-page components first and putting the header and footer around it. 

Javascript is developed in ES6 syntax, using features such as exports, imports, const, let, get functions etc. I decided that this is the most future-proof way to go. 

## The icons
The icons here, for purposes of speed alone, are [Elegant Icons](https://yarnpkg.com/en/package/elegant-icons-sass). There are a number of ways in which these are less than ideal - For example the sizing of the search icon and the menu icon is not at all consistent. Obviously, if this were being done for real, a good deal of time and attention would be devoted to making sure that the right icons were created and used. In this case, though, I had the afternoon to try and get something up and running from scratch, with nothing I could easily extract the used icons from, so I had to use a prebuilt icon pack.

## The header
The header is pretty basic at this time – It sticks to the top of the browser window and that's about it. The search item reveals a rudimentary search box when clicked (not the best way to do this for accessibility purposes, I know... Just needed something that worked for this example), which is automatically given focus.

## The hero component
Images within the hero component are in-line rather than being set with CSS as background images (whether inlined or otherwise). This improves printability of the page (because some people still do that kind of thing). 

You will note that this is all done in vanilla JS with no library dependencies. I wanted to show my scripting ability here, rather than ability to use someone else's work, which is what I would expect from a good coder. 

There is a basic progress indicator which runs along the bottom of the hero component. This is there, really, purely for the purposes of this exercise, just to show that the `mouseenter` and `mouseleave` states are being tracked by JS. 

The item indicators at the bottom right are fully functional, not only indicating the current slide, but allowing the user to jump to any item in the list. 

Most of the javascript for this whole component is pretty simple, functional stuff. All the effects and animations are dealt with in CSS using transitions. For a full project, I would like to move all the animations to javascript (despite common misconceptions, JS animations are significantly more performant if done right), and add additional effects to the slide text such as a gradual slide. 